/* $Id: symbol.c,v 1.1 2008/07/09 13:06:42 dvermeir Exp $
* 
* Symbol table management for toy ``TinyC'' language compiler.
* This is a trivial example: the only information kept
* is the name, the token type: READ, WRITE or NAME and, for NAMEs
* whether they have been declared in the JVM code.
*/
#include <stdio.h> /* for (f)printf(), std{out,int} */
#include <stdlib.h> /* for exit */
#include <string.h> /* for strcmp, strdup & friends */
#include "tinyc.tab.h" /* token type definitions from .y file */

#include "symbol.h"


typedef struct {
  char *name;
  char *function;
  int type;  /* READ, WRITE, or NAME */
  int declared; /* NAME only: 1 iff already declared in JVM code, 0 else */
  int offset;	/*offset from %ebp*/
} ENTRY;

#define  MAX_SYMBOLS 100
static ENTRY symbol_table[MAX_SYMBOLS]; /* not visible from outside */
static int n_symbol_table = 0; /* number of entries in symbol table */


int
symbol_find(char* name) {
  /* Find index of symbol table entry, -1 if not found */
  int i;

  for (i=0; (i<n_symbol_table); ++i)
    if (strcmp(symbol_table[i].name, name)==0)
      return i;
  return -1;
}

int
symbol_insert(char* name,int type) {
  /* Insert new symbol with a given type into symbol table,
   * returns index new value */
  if (n_symbol_table>=MAX_SYMBOLS) {
    fprintf(stderr, "Symbol table overflow (%d) entries\n", n_symbol_table);
    exit(1);
  }
  symbol_table[n_symbol_table].name = strdup(name);
  symbol_table[n_symbol_table].type = type;
  symbol_table[n_symbol_table].declared = 0;
  return n_symbol_table++;
}

void append_funcname(int i, char * funcname)
{
	symbol_table[i].function = funcname;
}

int
symbol_type(int i) {
  /* Return type of symbol at position i in table. */
  /* ASSERT ((0<=i)&&(i<n_symbol_table)) */
  return symbol_table[i].type;
}

void
symbol_declare(int i) {
  /* Mark a symbol in the table as declared */
  /* ASSERT ((0<=i)&&(i<n_symbol_table)&&(symbol_table[i].type==NAME)) */
  symbol_table[i].declared = 1;
}

int
symbol_declared(int i, char* func) {
  /* Return declared property of symbol */
  /* ASSERT ((0<=i)&&(i<n_symbol_table)&&(symbol_table[i].type==NAME)) */
  return symbol_table[i].declared && (strcmp(symbol_table[i].function, func) == 0);
}

int
function_declared(int i) {
  /* Return declared property of symbol */
  /* ASSERT ((0<=i)&&(i<n_symbol_table)&&(symbol_table[i].type==NAME)) */
  return symbol_table[i].declared;
}

char*
symbol_name(int i) {
  /* Return name of symbol */
  /* ASSERT ((0<=i)&&(i<n_symbol_table)) */
  return symbol_table[i].name;
}

void new_function(char *name) {
    printf(
".type %s, @function\n\
.globl %s\n\
%s:\n\
\tpushl %%ebp /* save base(frame) pointer on stack */\n\
\tmovl %%esp, %%ebp /* base pointer is stack pointer */\n\n", name, name, name);
}
//return the offset on the stack
int
symbol_offset(int i){
	return symbol_table[i].offset;
}
void set_offset(int i, int o){
	symbol_table[i].offset = o;
}

#define STACK_SIZE 50
int funcoffset_sp = 0;
int funcoffset_stack[STACK_SIZE];

void push_funcoffset(int theoffset)
{
    if (funcoffset_sp >= STACK_SIZE)
    {
        printf("Error: Offset stack overflow!;");
        exit(1);
        return;
    }
    
    funcoffset_stack[funcoffset_sp] = theoffset;
    funcoffset_sp++; // Stack Pointer can go up to STACK_SIZE, but no higher
}

int pop_funcoffset()
{
    if (funcoffset_sp < 0)
    {
        printf("Error: Offset Stack is empty!");
        exit(1);
        return;
    }
    
    funcoffset_sp--;
    return funcoffset_stack[funcoffset_sp + 1];
}

int paramcount_sp = 0;
int paramcount_stack[STACK_SIZE];

void push_paramcount(int theoffset)
{
    if (paramcount_sp >= STACK_SIZE)
    {
        fprintf(stderr, "Error: Params stack overflow!;");
        exit(1);
        return;
    }
    
    paramcount_stack[paramcount_sp] = theoffset;
    paramcount_sp++; // Stack Pointer can go up to STACK_SIZE, but no higher
}

int pop_paramcount()
{
    if (paramcount_sp < 0)
    {
        fprintf(stderr, "Error: Params Stack is empty!");
        exit(1);
        return;
    }
    
    paramcount_sp--;
    return paramcount_stack[paramcount_sp + 1];
}

int offset_sp = 0;
int offset_stack[STACK_SIZE];

void push_offset(int theoffset) {
    if (offset_sp >= STACK_SIZE)
    {
        fprintf(stderr, "Error: Params stack overflow!;");
        exit(1);
        return;
    }
    
    fprintf(stderr, "\t\tPushing %i onto offset_stack at %i\n", theoffset, offset_sp);
    
    offset_stack[offset_sp] = theoffset;
    offset_sp++; // Stack Pointer can go up to STACK_SIZE, but no higher
}

int pop_offset()
{
    if (offset_sp < 0)
    {
        fprintf(stderr, "Error: Params Stack is empty!");
        exit(1);
        return;
    }
    
    offset_sp--;
    fprintf(stderr, "\t\tPopping %i from offset_stack at %i\n", offset_stack[offset_sp], offset_sp);
    return offset_stack[offset_sp];
}

void overwriteHTML(char *filename, char* text){
	FILE * file;
	file = fopen(filename, "w");
	fputs(text, file);
	fclose(file);
}
void writeHTML(char *filename, char* text){
	FILE * file;
	file = fopen(filename, "a");
	fputs(text, file);
	fclose(file);
}
void writeNumHTML(char *filename, int num){

	FILE * file;
	file = fopen(filename, "a");
	fputs("<div style='width: 40px; display: inline-block; background-color: #CCC; text-align: right; margin-right: 5px; padding-right: 5px;'>#", file);
	fprintf(file, "%d", num);
	fputs("</div>", file);
	fclose(file);
}

void startdot(char* filename)
{
	FILE * file;
	file = fopen(filename, "w");
	
	fputs("digraph G {\n", file);
	fclose(file);
}

void outtodot(char* filename, char* text)
{
	FILE * file;
	file = fopen(filename, "a");
	fputs(text, file);
	fclose(file);
}

int num123=0;
int count1=0;
int count2=0;
char buffer [10];
void labelif1(){
    count1=count1+1;
    char s1[40]="IFLA";
    char s2[40]="ELSELA";
    char s2c[40]="ELSELA";
    snprintf(buffer, 5,"%d",count1);
    strncat (s2,buffer, 6);
    strncat (s2c,buffer, 6);
    strncat (s2,":\n",2);
    strncat (s2c,"\n",2);
    printf("\tcmpl %%ecx, %%eax\n");
    switch (num123) {
        case 1:
            printf("\tjge  %s\n",s2c);
            break;
        case 2:
            printf("\tjle  %s\n",s2c);
            break;
        case 3:
            printf("\tjne  %s\n",s2c);
            break;
        case 4:
            printf("\tje  %s\n",s2c);
            break;
        default:
            break;
    }

    snprintf(buffer, 5,"%d",count1);
    strncat (s1,buffer, 6);
    strncat (s1,":\n",2);
    //strcat (s1,buffer);
    //strcat (s1,"\n");
    printf( s1);
    
}


void else1(){
    char s3[40]="IFEND";
    snprintf(buffer, 5,"%d",count1);
    strncat (s3,buffer, 6);
    printf("\tjmp  %s\n",s3);
    char s2[40]="ELSELA";
    snprintf(buffer, 5,"%d",count1);
    strncat (s2,buffer, 6);
    strncat (s2,":\n",2);
    //strcat (s1,buffer);
    //strcat (s1,"\n");
    printf( s2);    
}

void else2(){
    char s3[40]="IFEND";
    snprintf(buffer, 5,"%d",count1);
    strncat (s3,buffer, 6);
    strncat (s3,":\n",2);
    printf( s3);    
}
void c1(){
    num123=1;
    
}
void c2(){
    num123=2;
    
}
void c3(){
    num123=3;
}

char html_file[50];
char dot_file[50];

extern char* get_html_file()
{
	return html_file;
}

extern void set_html_file(char* file)
{
	strcpy(html_file,file);
}

extern char* get_dot_file()
{
	return dot_file;
}

extern void set_dot_file(char* file)
{
	strcpy(dot_file,file);
}
void c4(){
    num123=4;
    
}

int wc=0;
int wi=0;

void w1(){


    char s1[40]="WB";

    char s2[40]="WE";

    snprintf(buffer, 5,"%d",wc);

    strncat (s2,buffer, 6);
    strncat (s2,":\n",2);

    snprintf(buffer, 5,"%d",wc);

    strncat (s1,buffer, 6);
    snprintf(buffer, 5,"%d",wi);
    strncat (s1,buffer, 6);

    strncat (s1,":\n",2);

    //strcat (s1,buffer);

    //strcat (s1,"\n");

    printf( s1);
}


void w2(){

    wc=wc-1;

    char s1[40]="WB";

    char s2[40]="WE";

    snprintf(buffer, 5,"%d",wc);

    strncat (s2,buffer, 6);

    snprintf(buffer, 5,"%d",wi);
    strncat (s2,buffer, 6);

    strncat (s2,":\n",2);



    

    snprintf(buffer, 5,"%d",wc);

    strncat (s1,buffer, 6);

    snprintf(buffer, 5,"%d",wi);
    strncat (s1,buffer, 6);

    strncat (s1,"\n",2);

    //strcat (s1,buffer);

    //strcat (s1,"\n");

    printf("\tjmp  %s\n",s1);

    printf( s2);
    if (wc==0){
 
             wi=wi+1;
     }
}

void w3(){

    char s1[40]="WB";

    char s2[40]="WE";

    snprintf(buffer, 5,"%d",wc);

    strncat (s2,buffer, 6);
    snprintf(buffer, 5,"%d",wi);
    strncat (s2,buffer, 6);
    strncat (s2,"\n",2);

    printf("\tcmpl %%ecx, %%eax\n");

    switch (num123) {
        case 1:
            printf("\tjge  %s\n",s2);
            break;
        case 2:
            printf("\tjle  %s\n",s2);
            break;
        case 3:
            printf("\tjne  %s\n",s2);
            break;
        case 4:
            printf("\tje  %s\n",s2);
            break;
        default:
            break;
    }
       wc=wc+1;

}

