%{
/* $Id: lex.l,v 1.1 2008/07/09 13:06:42 dvermeir Exp $
* 
* Lexical analyzer for the toy language ``TinyC''
*/
#include <string.h> /* for strcmp, strdup & friends */
#include <stdlib.h> /* for atoi() */

#include "tinyc.tab.h" /* token type definitions from .y file */
#include "symbol.h" /* symbol table management */

extern int lineno;  /* defined in tinyc.y */
//char *get_html_file();

void
lex_init() {
  /* Initialize data structures etc. for scanner */
	symbol_insert("declare",DECLARE); /*Insert keywords in symbol table */
    symbol_insert("read",READ); /*Insert keywords in symbol table */
    symbol_insert("write",WRITE);
    symbol_insert("int", INT);
    symbol_insert("return", RETURN);
    symbol_insert("if", IF);
}

/*
* The macro below will be called automatically when the generated scanner
* initializes itself.
*/
#define YY_USER_INIT lex_init();

%}



alpha   [A-Za-z]
digit   [0-9]
alphanum  [A-Za-z0-9]

%%
" "     break;  /* ignore white space */
"\t"    writeHTML(get_html_file(), "<div style='width: 20px; display: inline-block;'>&nbsp;</div>");
"\n"    writeHTML(get_html_file(), "<br>"); writeNumHTML(get_html_file(), lineno); ++lineno; 


"while"                 return WHILE;
"else"                  return ELSE;
"<"                     return SMALL;
">"                     return BIG;
"=="                    return EQUAL;
"!="                    return NE;

{alpha}{alphanum}*      {
                        yylval.idx = symbol_find(yytext);

                        if (yylval.idx<0) { /* new symbol: insert it */
                          yylval.idx =symbol_insert(yytext, NAME);
			  writeHTML(get_html_file(), "<span style = 'color: #00F; font-weight: bold;'>");
		  	writeHTML(get_html_file(), yytext);
   			writeHTML(get_html_file(), "</span>");
                          return NAME;
                        }
                        else{
				if (strcmp(yytext, "int")==0 || strcmp(yytext, "write")==0 || strcmp(yytext,"return")==0){
			  		writeHTML(get_html_file(), "<span style = 'color: #F00; font-weight: bold;'>");
					writeHTML(get_html_file(), yytext);
					writeHTML(get_html_file(), " ");
   					writeHTML(get_html_file(), "</span>");

				}
				else{

				  	writeHTML(get_html_file(), yytext);


				}
			    }
			 return symbol_type(yylval.idx);
                        }

{digit}+                {
                        yylval.value = atoi(yytext);
			writeHTML(get_html_file(), yytext);
			return NUMBER; 
                        }
"("                    writeHTML(get_html_file(), yytext); return LPAREN;
")"                    writeHTML(get_html_file(), yytext); return RPAREN;
"{"                    writeHTML(get_html_file(), yytext);  return LBRACE;
"}"                    writeHTML(get_html_file(), yytext);  return RBRACE;
"="                   writeHTML(get_html_file(), yytext); return ASSIGN;
","                    writeHTML(get_html_file(), yytext); return COMMA;
";"                    writeHTML(get_html_file(), yytext);  return SEMICOLON;
"+"                   writeHTML(get_html_file(), yytext); return PLUS;
"-"                    writeHTML(get_html_file(), yytext); return MINUS;
"*"			writeHTML(get_html_file(), yytext);return MUL;
"/"			writeHTML(get_html_file(), yytext);return DIV;
"%"			writeHTML(get_html_file(), yytext);return MOD;
"#".*		fprintf(stderr, yytext);

"//".*			{writeHTML(get_html_file(), "<span style = 'color: #0F0; font-weight: bold;'>");
			writeHTML(get_html_file(), yytext);
			writeHTML(get_html_file(), " ");
   			writeHTML(get_html_file(), "</span>");}
.                       { 
                        fprintf(stderr,
                                "Illegal character \'%s\' on line #%d\n",
                                yytext, lineno);
                        exit(1);
                        }

%%

int
yywrap() {
  return 1; /* tell scanner no further files need to be processed */
}

