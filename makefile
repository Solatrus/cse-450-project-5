# Special make rule to keep intermediate files *.s and *.o
# Comment out this line to have "cleaner" builds
.SECONDARY:

all:		tinyc

# Generate and run all test files
test:	t0 t1 t2 t3 t4 t5
	./t0
	./t1
	./t2
	./t3
	./t4
	./t5

# Build our tinyc Compiler using bison and flex files
#	$@	target name
#	$^	source files
#
tinyc:		tinyc.tab.o lex.yy.o symbol.o
		gcc -o $@ $^

# Bison options:
#	-v	Generate tinyc.output showing states of LALR parser
#	-d	Generate tinyc.tab.h containing token type definitions
#	$^	source files
#
tinyc.tab.h tinyc.tab.c:	tinyc.y
		bison -v -d $^

# Build flex
lex.yy.c:       lex.l tinyc.tab.h
		flex lex.l

########################################
# The following build rules use your compiler to build object files
#
# Requires tinyc source code:
#	filename.mi
#
# Usage:
#    make filename
########################################

# Use tinyc command to make assembly file *.s from *.tc files
#       %.s     Generic build rule for any assembly file basename
#       $@      target name
#       $<      first source files
%.s: %.tc tinyc
	./tinyc $< > $@
	dot -Tpng $<.dot -o $<.dot.png

# Convert assembly file *.s to object file *.o using gnu assembler (as)
#       %.o     Generic build rule for any object file basename
#       $@      target name
#       $<      first source files
%.o:    %.s
	as --32 $< -o $@

# Link Object file *.o to make executable
#	%	Generic build rule for any file basename
#	$@	target name
#	$^	source files
%:	%.o 
	ld -m elf_i386 $< -o $@ 

clean:
		rm -f demo lex.yy.c tinyc.tab.[hc] *.o tinyc tinyc.output *.s t? *.html *.png


