%{
/* $Id: tinyc.y,v 1.9 2008/07/11 18:56:43 dvermeir Exp $
*
* Parser specification for TinyC
*/
#include <stdio.h> /* for (f)printf() */
#include <stdlib.h> /* for exit() */
#include <string.h>

#include "symbol.h"

int offset = -4; /*keeps track of space on the stack for variables.  Probly want to change this later*/
int funcoffset = 0;
int paramcount = 0;

int  lineno = 1; /* number of current source line */
extern FILE* yyin;
extern int yylex(); /* lexical analyzer generated from lex.l */
extern char *yytext; /* last token, defined in lex.l  */

char* funcname;

char dot_file[50];
char html_file[50];

void
yyerror(char *s) {
  fprintf(stderr, "Syntax error on line #%d: %s\n", lineno, s);
  fprintf(stderr, "Last token was \"%s\"\n", yytext);
  exit(1);
}

#define PROLOGUE "\
.section .text\n\
.globl _start\n\
\n\
_start:\n\
	call main \n\
	jmp exit\n\
.include \"./x86asm/print_int.s\"\n\n\
"


#define EPILOGUE "\
.type exit, @function\n\
exit:\n\
	movl $0, %ebx\n\
	movl $1, %eax\n\
	int $0x80\n\
"

#define FUNC_END "	movl %ebp, %esp\n\
	popl %ebp /* restore old frame pointer */\n\
	ret\n\n"

%}

%union {
 int idx;
 int value;
 }

%token NAME
%token NUMBER
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token ASSIGN
%token SEMICOLON
%token COMMA
%token PLUS
%token MINUS
%token MUL
%token DIV
%token MOD
%token DECLARE
%token WRITE
%token READ
%token INT
%token RETURN
%token IF
%token ELSE
%token WHILE
%token SMALL
%token BIG
%token EQUAL
%token NE


%right ASSIGN
%left PLUS MINUS
%left MUL DIV MOD
%type <idx> NAME var
%type <value> NUMBER


%%
program         : func_dec_list { puts(EPILOGUE); writeHTML(html_file, "</body>\n</html>"); outtodot(dot_file, "}\n");}
func_dec_list   : function_dec func_dec_list
                | /* empty */
                  
function_dec    : INT NAME  {
                        if (function_declared($2)) {
                            //fprintf(stderr, "Function \"%s\" already declared (line %d)\n", symbol_name($2), lineno);
                            exit(1);
                        }
                        else {
                            new_function(symbol_name($2));
                            symbol_declare($2);
                            funcname = symbol_name($2);
                            push_paramcount(paramcount);
                            //fprintf(stderr, "%s:\n\tOffset before: %i\n",symbol_name($2),offset);
                            push_offset(offset);
                            offset = -4;
                            paramcount = 0;
                        }
                    } LPAREN param_decs { 
                        if (paramcount > 0) { 
                            push_funcoffset(funcoffset);
                            funcoffset = 8 + paramcount * 4;
                        }
                        fprintf(stderr, "\tParamcount: %i\n\tStack offset: %i\n", paramcount, funcoffset);
                    } RPAREN LBRACE declaration_list statement_list RBRACE {
                        puts(FUNC_END);
                        offset = pop_offset();
                        funcoffset = pop_funcoffset();
                        paramcount = pop_paramcount();
                        //fprintf(stderr, "\tOffset after: %i\n\n", offset);
                    };
                
param_decs      : param_declaration COMMA param_decs
                | param_declaration
                | /* empty */
                
param_declaration : INT NAME {
					//fprintf(stderr, "\tDeclaring %s in function %s\n", symbol_name($2), funcname);
                    if (symbol_declared($2, funcname)) {
                       fprintf(stderr, "Variable \"%s\" already declared (line %d)\n", symbol_name($2), lineno);
                       exit(1);
                    }
                    else {	

			
                        //puts("\taddl $-4, %esp /*Making space on stack for new var*/\n");
                        set_offset($2, offset);			
                        offset-=4;
                        paramcount += 1;
						append_funcname($2, funcname);
                        symbol_declare($2);
 
                    }
                    };
   
declaration_list  : local_declaration SEMICOLON declaration_list
                | /* empty */
                ;

statement_list  : statement SEMICOLON statement_list
                | if1 statement_list
                | w1 statement_list
                | /* empty */
                ;

statement       : assignment
                | read_statement
                | write_statement  
                | function_call  
                | local_declaration  
                | RETURN term { printf("\tpopl %%eax /*RETURN */\n"); }
                ;


w1              :WHILE  {w1();} LPAREN expression  {printf("\tpopl %%ecx\n");}COM1 expression {printf("\tpopl %%eax\n");} RPAREN LBRACE  {w3();}  statement_list RBRACE {w2(); } ;

if1             :ifstatement {else2();}
                | ifstatement elsestatement ;

ifstatement     :IF LPAREN expression { printf("\tpopl %%ecx\n"); } COM1 expression  { printf("\tpopl %%eax\n"); } RPAREN LBRACE {labelif1();}  statement_list RBRACE { else1(); }   ;


elsestatement     :ELSE  LBRACE statement_list RBRACE {else2();};

COM1              :BIG{c1();}|SMALL{c2();}|EQUAL{c3();}|NE{c4();};

assignment      : var ASSIGN expression

                  {
	            /* we assume that the expresion value is (%esp) */
                    //printf("\tpopl %s /* assignment */\n", symbol_name($1)); 
                    printf("\tpopl %d(", funcoffset + symbol_offset($1));
                    puts("%ebp) /*assignment*/");
                    //outtodot(" assigned to %s", symbol_name($1))
                  }
                ;

local_declaration: INT NAME 
                  { 
                     if (symbol_declared($2, funcname)) {
                       fprintf(stderr, "Variable \"%s\" already declared (line %d)\n", symbol_name($2), lineno);
                       exit(1);
                     }
                     else {
                     	if (paramcount > 0)
                     	{	
							set_offset($2, offset - 8);			
						}
						else
						{
							set_offset($2, offset);
						}
						offset-=4;
						append_funcname($2, funcname);
                        symbol_declare($2);
                        printf("\taddl $-4, %%esp /*Making space on stack for %s*/\n", symbol_name($2));


                     }
                  }


read_statement  : READ var { /* not implemented */}
                ;

write_statement : WRITE expression { puts("\tcall print_int\n"); }
                ;


expression      : term
                | function_call
		//Algebraic identity optimization.  If we add zero we don't want any addition done.
                | expression PLUS expression {if ($<value>3 != 0){puts("\tpopl %eax /*PLUS*/ \n\taddl %eax, (%esp)\n");}else{puts("\tpopl %eax /*PLUS*/ \n");}}
		//Algebraic identity optimization.  If we subtract zero we don't want any subtraction done.
                | expression MINUS expression {fprintf(stderr, "%i", $<value>3); if ($<value>3 != 0) {puts("\tpopl %eax /*MINUS*/ \n\tsubl %eax, (%esp)\n");} else{puts("\tpopl %eax /*MINUS*/ \n");} }
		//Algebraic identity optimization.  If we multiply by one we don't want any multiplication done.
                | expression MUL expression { if ($<value>3 != 1){puts("\tpopl %eax /*TIMES*/\n\tcltd"); puts("\timull (%esp)"); puts("\tmovl %eax, (%esp)\n");} else{puts("\tpopl %eax /*TIMES*/\n");}}
		//Algebraic identity optimization.  If we divide by one we don't want any division done.
                | expression DIV expression { if ($<value>3 != 1){puts("\tpopl %ebx /*DIVIDE*/"); puts("\tpopl %eax\n"); puts("\tcltd");puts("\tpush %ebx"); puts("\tidivl (%esp)"); puts("\tmovl %eax, (%esp)\n");} else{puts("\tpopl %ebx /*DIVIDE*/"); puts("\tpopl %eax\n"); puts("\tcltd");puts("\tpush %ebx");puts("\tmovl %eax, (%esp)\n");}}
                | expression MOD expression { puts("\tpopl %ebx /*DIVIDE*/\n"); puts("\tpopl %eax\n"); puts("\tcltd\n\tpush %ebx"); puts("\tidivl (%esp)"); puts("\tmovl %edx, (%esp)");}
                ;

term            : NUMBER { printf("\tpushl $%d /*Adding Constant to stack*/\n", $1); $<value>$ = $1; }
                | var {$<value>$ = 2; printf("\tpushl %d", funcoffset + symbol_offset($1)); printf("(%%ebp) /*adding %s to stack*/\n", symbol_name($1));}//  /*printf("\tpushl %s /* Adding  variable to stack*/ \n", symbol_name($1));*/ }
                | LPAREN expression RPAREN
                ;

var             : NAME
                  {
                    if (!symbol_declared($1, funcname)) {
                      fprintf(stderr, "Variable \"%s\" not declared (line %d)\n", symbol_name($1), lineno);
                      exit(1);
                    }

                    $$ = $1;
                  }
                ;
                
function_call   : NAME LPAREN param_list RPAREN {
					printf("\tcall %s\n\tpushl %%eax\n", symbol_name($1));
					char output[50];
					strcpy(output,"\t");
					strcat(output, funcname);
					//fprintf(stderr, "Welp.\n");
					strcat(output, " -> ");
					strcat(output, symbol_name($1));
					strcat(output, ";\n");
					//asprintf(output, "\t%s -> %s;\n", funcname, symbol_name($1));
					//fprintf(stderr, output);
					outtodot(dot_file, output);
				};

param_list      : expression
                | expression COMMA param_list
				| /* empty */;
%%

int main(int argc,char *argv[]) {
	//fprintf(stderr, "%i arguments...\n\n", argc);
	if (argc > 1) {
		yyin = fopen(argv[1], "r");
		strcpy(html_file,argv[1]);
		strcpy(dot_file,argv[1]);
		strcat(html_file,".html");
		strcat(dot_file,".dot");
	}
	else {
		strcpy(html_file,"test.tc.html");
		strcpy(dot_file,"test.tc.dot");
	}
	
	set_html_file(html_file);
	set_dot_file(dot_file);
	
	overwriteHTML(html_file, "<html>\n<head>Tiny C Code</head>\n<body>\n");
    startdot(dot_file);
    puts(PROLOGUE);
    return yyparse();
} 
