#ifndef	SYMBOL_H
#define	SYMBOL_H
/* $Id: symbol.h,v 1.1 2008/07/09 13:06:42 dvermeir Exp $
* 
* Symbol table management for toy ``TinyC'' language compiler.
*/
extern int symbol_insert(char* name,int type);
extern void symbol_declare(int i);

extern int symbol_find(char* name);
extern char* symbol_name(int i);
extern int symbol_type(int i);
extern int symbol_declared(int i, char *func);
extern int function_declared(int i);
extern void new_function(char* name);

extern void outtodot(char* filename, char* text);
extern void startdot(char* filename);

extern char* get_html_file();
extern void set_html_file(char* file);

extern char* get_dot_file();
extern void set_dot_file();
#endif
